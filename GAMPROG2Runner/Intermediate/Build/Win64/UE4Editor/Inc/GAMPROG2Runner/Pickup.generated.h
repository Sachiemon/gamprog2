// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef GAMPROG2RUNNER_Pickup_generated_h
#error "Pickup.generated.h already included, missing '#pragma once' in Pickup.h"
#endif
#define GAMPROG2RUNNER_Pickup_generated_h

#define GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_SPARSE_DATA
#define GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnPickUp);


#define GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnPickUp);


#define GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_EVENT_PARMS
#define GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_CALLBACK_WRAPPERS
#define GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPickup(); \
	friend struct Z_Construct_UClass_APickup_Statics; \
public: \
	DECLARE_CLASS(APickup, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GAMPROG2Runner"), NO_API) \
	DECLARE_SERIALIZER(APickup)


#define GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPickup(); \
	friend struct Z_Construct_UClass_APickup_Statics; \
public: \
	DECLARE_CLASS(APickup, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GAMPROG2Runner"), NO_API) \
	DECLARE_SERIALIZER(APickup)


#define GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APickup(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APickup) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APickup); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APickup); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APickup(APickup&&); \
	NO_API APickup(const APickup&); \
public:


#define GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APickup(APickup&&); \
	NO_API APickup(const APickup&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APickup); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APickup); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APickup)


#define GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh() { return STRUCT_OFFSET(APickup, Mesh); } \
	FORCEINLINE static uint32 __PPO__SceneComponent() { return STRUCT_OFFSET(APickup, SceneComponent); }


#define GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_9_PROLOG \
	GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_EVENT_PARMS


#define GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_PRIVATE_PROPERTY_OFFSET \
	GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_SPARSE_DATA \
	GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_RPC_WRAPPERS \
	GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_CALLBACK_WRAPPERS \
	GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_INCLASS \
	GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_PRIVATE_PROPERTY_OFFSET \
	GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_SPARSE_DATA \
	GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_CALLBACK_WRAPPERS \
	GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_INCLASS_NO_PURE_DECLS \
	GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPROG2RUNNER_API UClass* StaticClass<class APickup>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAMPROG2Runner_Source_GAMPROG2Runner_Pickup_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
