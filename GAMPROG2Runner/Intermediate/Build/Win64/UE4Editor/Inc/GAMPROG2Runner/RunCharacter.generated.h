// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPROG2RUNNER_RunCharacter_generated_h
#error "RunCharacter.generated.h already included, missing '#pragma once' in RunCharacter.h"
#endif
#define GAMPROG2RUNNER_RunCharacter_generated_h

#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_10_DELEGATE \
static inline void FDeathTriggerSignature_DelegateWrapper(const FMulticastScriptDelegate& DeathTriggerSignature) \
{ \
	DeathTriggerSignature.ProcessMulticastDelegate<UObject>(NULL); \
}


#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_17_SPARSE_DATA
#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAddCoins); \
	DECLARE_FUNCTION(execDie);


#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAddCoins); \
	DECLARE_FUNCTION(execDie);


#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARunCharacter(); \
	friend struct Z_Construct_UClass_ARunCharacter_Statics; \
public: \
	DECLARE_CLASS(ARunCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GAMPROG2Runner"), NO_API) \
	DECLARE_SERIALIZER(ARunCharacter)


#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_17_INCLASS \
private: \
	static void StaticRegisterNativesARunCharacter(); \
	friend struct Z_Construct_UClass_ARunCharacter_Statics; \
public: \
	DECLARE_CLASS(ARunCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GAMPROG2Runner"), NO_API) \
	DECLARE_SERIALIZER(ARunCharacter)


#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARunCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARunCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunCharacter(ARunCharacter&&); \
	NO_API ARunCharacter(const ARunCharacter&); \
public:


#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunCharacter(ARunCharacter&&); \
	NO_API ARunCharacter(const ARunCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARunCharacter)


#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SpringArm() { return STRUCT_OFFSET(ARunCharacter, SpringArm); } \
	FORCEINLINE static uint32 __PPO__Camera() { return STRUCT_OFFSET(ARunCharacter, Camera); } \
	FORCEINLINE static uint32 __PPO__Movement() { return STRUCT_OFFSET(ARunCharacter, Movement); } \
	FORCEINLINE static uint32 __PPO__MeshComponent() { return STRUCT_OFFSET(ARunCharacter, MeshComponent); }


#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_14_PROLOG
#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_17_PRIVATE_PROPERTY_OFFSET \
	GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_17_SPARSE_DATA \
	GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_17_RPC_WRAPPERS \
	GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_17_INCLASS \
	GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_17_PRIVATE_PROPERTY_OFFSET \
	GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_17_SPARSE_DATA \
	GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_17_INCLASS_NO_PURE_DECLS \
	GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPROG2RUNNER_API UClass* StaticClass<class ARunCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
