// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPROG2RUNNER_GAMPROG2RunnerGameMode_generated_h
#error "GAMPROG2RunnerGameMode.generated.h already included, missing '#pragma once' in GAMPROG2RunnerGameMode.h"
#endif
#define GAMPROG2RUNNER_GAMPROG2RunnerGameMode_generated_h

#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h_12_SPARSE_DATA
#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h_12_RPC_WRAPPERS
#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGAMPROG2RunnerGameMode(); \
	friend struct Z_Construct_UClass_AGAMPROG2RunnerGameMode_Statics; \
public: \
	DECLARE_CLASS(AGAMPROG2RunnerGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/GAMPROG2Runner"), GAMPROG2RUNNER_API) \
	DECLARE_SERIALIZER(AGAMPROG2RunnerGameMode)


#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAGAMPROG2RunnerGameMode(); \
	friend struct Z_Construct_UClass_AGAMPROG2RunnerGameMode_Statics; \
public: \
	DECLARE_CLASS(AGAMPROG2RunnerGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/GAMPROG2Runner"), GAMPROG2RUNNER_API) \
	DECLARE_SERIALIZER(AGAMPROG2RunnerGameMode)


#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	GAMPROG2RUNNER_API AGAMPROG2RunnerGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGAMPROG2RunnerGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(GAMPROG2RUNNER_API, AGAMPROG2RunnerGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAMPROG2RunnerGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	GAMPROG2RUNNER_API AGAMPROG2RunnerGameMode(AGAMPROG2RunnerGameMode&&); \
	GAMPROG2RUNNER_API AGAMPROG2RunnerGameMode(const AGAMPROG2RunnerGameMode&); \
public:


#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	GAMPROG2RUNNER_API AGAMPROG2RunnerGameMode(AGAMPROG2RunnerGameMode&&); \
	GAMPROG2RUNNER_API AGAMPROG2RunnerGameMode(const AGAMPROG2RunnerGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(GAMPROG2RUNNER_API, AGAMPROG2RunnerGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAMPROG2RunnerGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGAMPROG2RunnerGameMode)


#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h_9_PROLOG
#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h_12_SPARSE_DATA \
	GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h_12_RPC_WRAPPERS \
	GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h_12_INCLASS \
	GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h_12_SPARSE_DATA \
	GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h_12_INCLASS_NO_PURE_DECLS \
	GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPROG2RUNNER_API UClass* StaticClass<class AGAMPROG2RunnerGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
