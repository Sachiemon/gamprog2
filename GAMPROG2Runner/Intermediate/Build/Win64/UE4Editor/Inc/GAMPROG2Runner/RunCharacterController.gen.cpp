// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GAMPROG2Runner/RunCharacterController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRunCharacterController() {}
// Cross Module References
	GAMPROG2RUNNER_API UClass* Z_Construct_UClass_ARunCharacterController_NoRegister();
	GAMPROG2RUNNER_API UClass* Z_Construct_UClass_ARunCharacterController();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_GAMPROG2Runner();
	GAMPROG2RUNNER_API UClass* Z_Construct_UClass_ARunCharacter_NoRegister();
// End Cross Module References
	void ARunCharacterController::StaticRegisterNativesARunCharacterController()
	{
	}
	UClass* Z_Construct_UClass_ARunCharacterController_NoRegister()
	{
		return ARunCharacterController::StaticClass();
	}
	struct Z_Construct_UClass_ARunCharacterController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RunCharacterRef_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RunCharacterRef;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_moveSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_moveSpeed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ARunCharacterController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_GAMPROG2Runner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARunCharacterController_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "RunCharacterController.h" },
		{ "ModuleRelativePath", "RunCharacterController.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARunCharacterController_Statics::NewProp_RunCharacterRef_MetaData[] = {
		{ "Category", "RunCharacterController" },
		{ "ModuleRelativePath", "RunCharacterController.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARunCharacterController_Statics::NewProp_RunCharacterRef = { "RunCharacterRef", nullptr, (EPropertyFlags)0x0020080000020015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARunCharacterController, RunCharacterRef), Z_Construct_UClass_ARunCharacter_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARunCharacterController_Statics::NewProp_RunCharacterRef_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARunCharacterController_Statics::NewProp_RunCharacterRef_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARunCharacterController_Statics::NewProp_moveSpeed_MetaData[] = {
		{ "Category", "RunCharacterController" },
		{ "ModuleRelativePath", "RunCharacterController.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ARunCharacterController_Statics::NewProp_moveSpeed = { "moveSpeed", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARunCharacterController, moveSpeed), METADATA_PARAMS(Z_Construct_UClass_ARunCharacterController_Statics::NewProp_moveSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARunCharacterController_Statics::NewProp_moveSpeed_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ARunCharacterController_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARunCharacterController_Statics::NewProp_RunCharacterRef,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARunCharacterController_Statics::NewProp_moveSpeed,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ARunCharacterController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ARunCharacterController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ARunCharacterController_Statics::ClassParams = {
		&ARunCharacterController::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ARunCharacterController_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ARunCharacterController_Statics::PropPointers),
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_ARunCharacterController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ARunCharacterController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ARunCharacterController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ARunCharacterController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ARunCharacterController, 3678967003);
	template<> GAMPROG2RUNNER_API UClass* StaticClass<ARunCharacterController>()
	{
		return ARunCharacterController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ARunCharacterController(Z_Construct_UClass_ARunCharacterController, &ARunCharacterController::StaticClass, TEXT("/Script/GAMPROG2Runner"), TEXT("ARunCharacterController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ARunCharacterController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
