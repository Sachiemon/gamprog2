// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UPrimitiveComponent;
struct FHitResult;
#ifdef GAMPROG2RUNNER_Obstacle_generated_h
#error "Obstacle.generated.h already included, missing '#pragma once' in Obstacle.h"
#endif
#define GAMPROG2RUNNER_Obstacle_generated_h

#define GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_SPARSE_DATA
#define GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_RPC_WRAPPERS
#define GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_EVENT_PARMS \
	struct Obstacle_eventOnTrigger_Parms \
	{ \
		AActor* OtherActor; \
		UPrimitiveComponent* OtherComp; \
		int32 OtherBodyIndex; \
		bool bFromSweep; \
		FHitResult SweepResult; \
	};


#define GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_CALLBACK_WRAPPERS
#define GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAObstacle(); \
	friend struct Z_Construct_UClass_AObstacle_Statics; \
public: \
	DECLARE_CLASS(AObstacle, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GAMPROG2Runner"), NO_API) \
	DECLARE_SERIALIZER(AObstacle)


#define GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAObstacle(); \
	friend struct Z_Construct_UClass_AObstacle_Statics; \
public: \
	DECLARE_CLASS(AObstacle, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GAMPROG2Runner"), NO_API) \
	DECLARE_SERIALIZER(AObstacle)


#define GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AObstacle(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AObstacle) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AObstacle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AObstacle); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AObstacle(AObstacle&&); \
	NO_API AObstacle(const AObstacle&); \
public:


#define GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AObstacle(AObstacle&&); \
	NO_API AObstacle(const AObstacle&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AObstacle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AObstacle); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AObstacle)


#define GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_PRIVATE_PROPERTY_OFFSET
#define GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_9_PROLOG \
	GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_EVENT_PARMS


#define GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_PRIVATE_PROPERTY_OFFSET \
	GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_SPARSE_DATA \
	GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_RPC_WRAPPERS \
	GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_CALLBACK_WRAPPERS \
	GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_INCLASS \
	GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_PRIVATE_PROPERTY_OFFSET \
	GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_SPARSE_DATA \
	GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_CALLBACK_WRAPPERS \
	GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_INCLASS_NO_PURE_DECLS \
	GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPROG2RUNNER_API UClass* StaticClass<class AObstacle>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAMPROG2Runner_Source_GAMPROG2Runner_Obstacle_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
