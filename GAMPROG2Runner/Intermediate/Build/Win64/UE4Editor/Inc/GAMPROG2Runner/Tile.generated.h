// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ATile;
#ifdef GAMPROG2RUNNER_Tile_generated_h
#error "Tile.generated.h already included, missing '#pragma once' in Tile.h"
#endif
#define GAMPROG2RUNNER_Tile_generated_h

#define GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_15_DELEGATE \
struct _Script_GAMPROG2Runner_eventExitTriggerSignature_Parms \
{ \
	ATile* Tile; \
}; \
static inline void FExitTriggerSignature_DelegateWrapper(const FMulticastScriptDelegate& ExitTriggerSignature, ATile* Tile) \
{ \
	_Script_GAMPROG2Runner_eventExitTriggerSignature_Parms Parms; \
	Parms.Tile=Tile; \
	ExitTriggerSignature.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_22_SPARSE_DATA
#define GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_22_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execDestroyTile);


#define GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execDestroyTile);


#define GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATile(); \
	friend struct Z_Construct_UClass_ATile_Statics; \
public: \
	DECLARE_CLASS(ATile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GAMPROG2Runner"), NO_API) \
	DECLARE_SERIALIZER(ATile)


#define GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_22_INCLASS \
private: \
	static void StaticRegisterNativesATile(); \
	friend struct Z_Construct_UClass_ATile_Statics; \
public: \
	DECLARE_CLASS(ATile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GAMPROG2Runner"), NO_API) \
	DECLARE_SERIALIZER(ATile)


#define GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATile(ATile&&); \
	NO_API ATile(const ATile&); \
public:


#define GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATile(ATile&&); \
	NO_API ATile(const ATile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATile)


#define GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_22_PRIVATE_PROPERTY_OFFSET
#define GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_19_PROLOG
#define GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_22_PRIVATE_PROPERTY_OFFSET \
	GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_22_SPARSE_DATA \
	GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_22_RPC_WRAPPERS \
	GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_22_INCLASS \
	GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_22_PRIVATE_PROPERTY_OFFSET \
	GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_22_SPARSE_DATA \
	GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_22_INCLASS_NO_PURE_DECLS \
	GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPROG2RUNNER_API UClass* StaticClass<class ATile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAMPROG2Runner_Source_GAMPROG2Runner_Tile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
