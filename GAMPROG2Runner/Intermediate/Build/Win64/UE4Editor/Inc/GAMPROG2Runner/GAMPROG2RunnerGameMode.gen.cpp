// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GAMPROG2Runner/GAMPROG2RunnerGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGAMPROG2RunnerGameMode() {}
// Cross Module References
	GAMPROG2RUNNER_API UClass* Z_Construct_UClass_AGAMPROG2RunnerGameMode_NoRegister();
	GAMPROG2RUNNER_API UClass* Z_Construct_UClass_AGAMPROG2RunnerGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_GAMPROG2Runner();
// End Cross Module References
	void AGAMPROG2RunnerGameMode::StaticRegisterNativesAGAMPROG2RunnerGameMode()
	{
	}
	UClass* Z_Construct_UClass_AGAMPROG2RunnerGameMode_NoRegister()
	{
		return AGAMPROG2RunnerGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AGAMPROG2RunnerGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AGAMPROG2RunnerGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_GAMPROG2Runner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGAMPROG2RunnerGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "GAMPROG2RunnerGameMode.h" },
		{ "ModuleRelativePath", "GAMPROG2RunnerGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AGAMPROG2RunnerGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AGAMPROG2RunnerGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AGAMPROG2RunnerGameMode_Statics::ClassParams = {
		&AGAMPROG2RunnerGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_AGAMPROG2RunnerGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AGAMPROG2RunnerGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AGAMPROG2RunnerGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AGAMPROG2RunnerGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AGAMPROG2RunnerGameMode, 3204386905);
	template<> GAMPROG2RUNNER_API UClass* StaticClass<AGAMPROG2RunnerGameMode>()
	{
		return AGAMPROG2RunnerGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGAMPROG2RunnerGameMode(Z_Construct_UClass_AGAMPROG2RunnerGameMode, &AGAMPROG2RunnerGameMode::StaticClass, TEXT("/Script/GAMPROG2Runner"), TEXT("AGAMPROG2RunnerGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGAMPROG2RunnerGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
