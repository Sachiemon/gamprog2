// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPROG2RUNNER_GAMPROG2RunnerCharacter_generated_h
#error "GAMPROG2RunnerCharacter.generated.h already included, missing '#pragma once' in GAMPROG2RunnerCharacter.h"
#endif
#define GAMPROG2RUNNER_GAMPROG2RunnerCharacter_generated_h

#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h_12_SPARSE_DATA
#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h_12_RPC_WRAPPERS
#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGAMPROG2RunnerCharacter(); \
	friend struct Z_Construct_UClass_AGAMPROG2RunnerCharacter_Statics; \
public: \
	DECLARE_CLASS(AGAMPROG2RunnerCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GAMPROG2Runner"), NO_API) \
	DECLARE_SERIALIZER(AGAMPROG2RunnerCharacter)


#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAGAMPROG2RunnerCharacter(); \
	friend struct Z_Construct_UClass_AGAMPROG2RunnerCharacter_Statics; \
public: \
	DECLARE_CLASS(AGAMPROG2RunnerCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GAMPROG2Runner"), NO_API) \
	DECLARE_SERIALIZER(AGAMPROG2RunnerCharacter)


#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGAMPROG2RunnerCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGAMPROG2RunnerCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGAMPROG2RunnerCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAMPROG2RunnerCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGAMPROG2RunnerCharacter(AGAMPROG2RunnerCharacter&&); \
	NO_API AGAMPROG2RunnerCharacter(const AGAMPROG2RunnerCharacter&); \
public:


#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGAMPROG2RunnerCharacter(AGAMPROG2RunnerCharacter&&); \
	NO_API AGAMPROG2RunnerCharacter(const AGAMPROG2RunnerCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGAMPROG2RunnerCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGAMPROG2RunnerCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGAMPROG2RunnerCharacter)


#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AGAMPROG2RunnerCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(AGAMPROG2RunnerCharacter, FollowCamera); }


#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h_9_PROLOG
#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h_12_SPARSE_DATA \
	GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h_12_RPC_WRAPPERS \
	GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h_12_INCLASS \
	GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h_12_SPARSE_DATA \
	GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h_12_INCLASS_NO_PURE_DECLS \
	GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPROG2RUNNER_API UClass* StaticClass<class AGAMPROG2RunnerCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAMPROG2Runner_Source_GAMPROG2Runner_GAMPROG2RunnerCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
