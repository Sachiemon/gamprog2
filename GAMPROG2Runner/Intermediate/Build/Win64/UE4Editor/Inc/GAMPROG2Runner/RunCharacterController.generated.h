// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPROG2RUNNER_RunCharacterController_generated_h
#error "RunCharacterController.generated.h already included, missing '#pragma once' in RunCharacterController.h"
#endif
#define GAMPROG2RUNNER_RunCharacterController_generated_h

#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h_17_SPARSE_DATA
#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h_17_RPC_WRAPPERS
#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARunCharacterController(); \
	friend struct Z_Construct_UClass_ARunCharacterController_Statics; \
public: \
	DECLARE_CLASS(ARunCharacterController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GAMPROG2Runner"), NO_API) \
	DECLARE_SERIALIZER(ARunCharacterController)


#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h_17_INCLASS \
private: \
	static void StaticRegisterNativesARunCharacterController(); \
	friend struct Z_Construct_UClass_ARunCharacterController_Statics; \
public: \
	DECLARE_CLASS(ARunCharacterController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GAMPROG2Runner"), NO_API) \
	DECLARE_SERIALIZER(ARunCharacterController)


#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARunCharacterController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARunCharacterController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunCharacterController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunCharacterController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunCharacterController(ARunCharacterController&&); \
	NO_API ARunCharacterController(const ARunCharacterController&); \
public:


#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunCharacterController(ARunCharacterController&&); \
	NO_API ARunCharacterController(const ARunCharacterController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunCharacterController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunCharacterController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARunCharacterController)


#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__moveSpeed() { return STRUCT_OFFSET(ARunCharacterController, moveSpeed); } \
	FORCEINLINE static uint32 __PPO__RunCharacterRef() { return STRUCT_OFFSET(ARunCharacterController, RunCharacterRef); }


#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h_14_PROLOG
#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h_17_PRIVATE_PROPERTY_OFFSET \
	GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h_17_SPARSE_DATA \
	GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h_17_RPC_WRAPPERS \
	GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h_17_INCLASS \
	GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h_17_PRIVATE_PROPERTY_OFFSET \
	GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h_17_SPARSE_DATA \
	GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h_17_INCLASS_NO_PURE_DECLS \
	GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPROG2RUNNER_API UClass* StaticClass<class ARunCharacterController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAMPROG2Runner_Source_GAMPROG2Runner_RunCharacterController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
