// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGAMPROG2Runner_init() {}
	GAMPROG2RUNNER_API UFunction* Z_Construct_UDelegateFunction_GAMPROG2Runner_DeathTriggerSignature__DelegateSignature();
	GAMPROG2RUNNER_API UFunction* Z_Construct_UDelegateFunction_GAMPROG2Runner_ExitTriggerSignature__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_GAMPROG2Runner()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_GAMPROG2Runner_DeathTriggerSignature__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_GAMPROG2Runner_ExitTriggerSignature__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/GAMPROG2Runner",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x65A03C64,
				0x6E35068B,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
