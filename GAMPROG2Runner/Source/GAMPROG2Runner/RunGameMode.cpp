// Fill out your copyright notice in the Description page of Project Settings.


#include "RunGameMode.h"
#include "Tile.h"
#include "RunCharacter.h"
void ARunGameMode::CreateInitialFloorTile()
{
	for (int i = 0; i < InitialFloorTiles; i++)
	{
		AddFloor();
	}
}

void ARunGameMode::AddFloor()
{
	UWorld* World = GetWorld();
	if (World)
	{
		ATile* Tile = World->SpawnActor<ATile>(TileClass, SpawnPoint);
		if (Tile)
		{
			SpawnPoint = Tile->GetAttachTransform();
			Tile->OnExited.AddDynamic(this, &ARunGameMode::OnExited);
		}
	}


}

void ARunGameMode::BeginPlay()
{
	Super::BeginPlay();
	CreateInitialFloorTile();

}
void ARunGameMode::OnExited(ATile* Tile)
{
	AddFloor();

}

