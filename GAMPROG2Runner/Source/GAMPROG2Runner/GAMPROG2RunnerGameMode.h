// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GAMPROG2RunnerGameMode.generated.h"

UCLASS(minimalapi)
class AGAMPROG2RunnerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGAMPROG2RunnerGameMode();
};



