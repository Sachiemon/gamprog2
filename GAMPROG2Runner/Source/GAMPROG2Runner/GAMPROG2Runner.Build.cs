// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class GAMPROG2Runner : ModuleRules
{
	public GAMPROG2Runner(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
