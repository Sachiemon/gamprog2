// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Tile.h"
#include "RunGameMode.generated.h"


/**
 * 
 */
UCLASS()
class GAMPROG2RUNNER_API ARunGameMode : public AGameModeBase
{
    GENERATED_BODY()

public:
    ATile* recentTile;
    FORCEINLINE ATile* GetrecentTile()
    {
        return recentTile;
    }
    UPROPERTY(EditAnywhere,BlueprintReadOnly, Category = "Configuration")
        TSubclassOf<ATile> TileClass;
    UPROPERTY(EditAnywhere, Category = "Configuration")
        int32 InitialFloorTiles = 10;
    UPROPERTY(VisibleInstanceOnly, Category = "Runtime")
        FTransform SpawnPoint;
    UFUNCTION(BlueprintCallable)
        void CreateInitialFloorTile();
    UFUNCTION(BlueprintCallable)
        void AddFloor();
    UFUNCTION()
    void OnExited(class ATile* tile);
protected:
    virtual void BeginPlay() override;

};

