// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/InputComponent.h"
#include "RunCharacter.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "RunCharacterController.h"

ARunCharacterController::ARunCharacterController()
{
	
}
void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();
	RunCharacterRef = Cast<ARunCharacter>(GetPawn());
	
}
void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();
	
	//InputComponent->BindAxis("MoveForward", this, &ARunCharacterController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveRight);
	
}
void ARunCharacterController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	MoveForward(1.0f);
}
void ARunCharacterController::MoveForward(float scale)
{
	RunCharacterRef->AddMovementInput(RunCharacterRef->GetActorForwardVector() * scale);
}

void ARunCharacterController::MoveRight(float scale)
{
	RunCharacterRef->AddMovementInput(RunCharacterRef->GetActorRightVector() * scale * moveSpeed);
}



