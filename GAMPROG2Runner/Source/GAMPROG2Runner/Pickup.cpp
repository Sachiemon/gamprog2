// Fill out your copyright notice in the Description page of Project Settings.

#include "RunCharacter.h"
#include "Pickup.h"



// Sets default values
APickup::APickup()
{
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	RootComponent = SceneComponent;
	Mesh->SetupAttachment(GetRootComponent());
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APickup::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void APickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
void APickup::OnPickUp(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ARunCharacter* Runcharacter= Cast<ARunCharacter>(OtherActor);
	OnGet();
	Destroy();
}



