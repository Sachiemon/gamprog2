// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "Pickup.h"
#include "Obstacle.h"
#include "Tile.generated.h"



DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FExitTriggerSignature, ATile*, Tile);



UCLASS()
class GAMPROG2RUNNER_API ATile : public AActor
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Configuration")
		TArray<TSubclassOf<AObstacle>> ObstacleArray;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Configuration")
		TArray<TSubclassOf<APickup>> PickupArray;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class USceneComponent* SceneComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class UStaticMeshComponent* FloorMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class UArrowComponent* AttachPoint;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class UBoxComponent* ExitTrigger;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class UBoxComponent* ObstacleSpawn;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class UBoxComponent* PickupSpawn;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool Spawned = false;
	// Sets default values for this actor's properties
	ATile();
	FTimerHandle DestroyHandle;
	UFUNCTION()
	void DestroyTile();
	//UFUNCTION(BlueprintImplementableEvent)
	void OnExit(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	UPROPERTY(BlueprintAssignable,Category="EventDispatcher")
	FExitTriggerSignature OnExited;
	FORCEINLINE const FTransform& GetAttachTransform() const
	{
		return AttachPoint->GetComponentTransform();
	}
protected:
	class ARunGameMode* RunGameMode;

	void SpawnPickup();
	void SpawnObstacle();
		
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
