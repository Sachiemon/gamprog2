// Copyright Epic Games, Inc. All Rights Reserved.

#include "GAMPROG2RunnerGameMode.h"
#include "GAMPROG2RunnerCharacter.h"
#include "UObject/ConstructorHelpers.h"

AGAMPROG2RunnerGameMode::AGAMPROG2RunnerGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
