// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/InputComponent.h"
#include "RunCharacter.h"
#include "GameFramework/PlayerController.h"
#include "RunCharacterController.generated.h"

/**
 * 
 */
UCLASS()
class GAMPROG2RUNNER_API ARunCharacterController : public APlayerController
{
	GENERATED_BODY()

public:

	ARunCharacterController();


protected:
	virtual void BeginPlay() override;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float moveSpeed;
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly)
	class ARunCharacter* RunCharacterRef;
	//Input Bindings
	void MoveForward(float scale);
	void MoveRight(float scale); 
public:
	virtual void Tick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	
};
