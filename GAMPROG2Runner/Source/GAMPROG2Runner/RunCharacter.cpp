// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Obstacle.h"
#include "Pickup.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "Math/Vector.h"

// Sets default values
ARunCharacter::ARunCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(("SpringArmComponent"));
	SpringArm->SetRelativeLocation(FVector(0.0f, 0.0f, 100.0f));
	SpringArm->bUsePawnControlRotation = true;
	SpringArm->SetupAttachment(GetRootComponent());
	Camera = CreateDefaultSubobject<UCameraComponent>(("CameraComponent"));
	Camera->bUsePawnControlRotation = false;
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(("MeshComponent"));
	MeshComponent->SetupAttachment(GetRootComponent());

	Coins = 0;
}

// Called when the game starts or when spawned
void ARunCharacter::BeginPlay()
{
	Super::BeginPlay();
	isAlive = true;
	

	
}

void ARunCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
void ARunCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}



void ARunCharacter::Die()
{
	if (!isAlive)
	{
		return;
		}
		isAlive = false;
		GetMesh()->SetVisibility(false);
		APlayerController* PlayerController = Cast<APlayerController>(GetController());
		this->DisableInput(PlayerController);	
		OnDeath.Broadcast();

}

void ARunCharacter::AddCoins()
{
	Coins++;
}

	