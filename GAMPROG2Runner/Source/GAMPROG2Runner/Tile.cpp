// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "RunGameMode.h"
#include "Kismet/KismetMathLibrary.h"
#include "RunCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Obstacle.h"
#include "TimerManager.h"


// Sets default values
ATile::ATile()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(("RootSceneComponent"));
	RootComponent = SceneComponent;
	FloorMesh = CreateDefaultSubobject<UStaticMeshComponent>(("StaticMeshComponent"));
	FloorMesh->SetupAttachment(GetRootComponent());
	AttachPoint = CreateDefaultSubobject<UArrowComponent>(("AttachPoint"));
	AttachPoint->SetupAttachment((GetRootComponent()));
	ExitTrigger = CreateDefaultSubobject<UBoxComponent>(("ExitTrigger"));
	ExitTrigger->SetupAttachment((GetRootComponent()));
	ObstacleSpawn = CreateDefaultSubobject<UBoxComponent>(("ObstacleSpawn"));
	PickupSpawn = CreateDefaultSubobject<UBoxComponent>(("PickupSpawn"));
	


}



// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
	RunGameMode = Cast<ARunGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	check(RunGameMode);
	ExitTrigger->OnComponentEndOverlap.AddDynamic(this, &ATile::OnExit);
	//SpawnObstacle();
	//SpawnPickup();

}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}



void ATile::DestroyTile()
{
	
	GetWorldTimerManager().ClearTimer(DestroyHandle);
	
	this->Destroy();
}
void ATile::SpawnObstacle()
{

	int32 Number = ObstacleArray.Num()-1;
	const int RandObstacle = FMath::FRandRange(0, Number);
	TSubclassOf<AObstacle> ObstacleToSpawn = ObstacleArray[RandObstacle];
	FVector spawnPosition =UKismetMathLibrary::RandomPointInBoundingBox(ObstacleSpawn->GetComponentLocation(), ObstacleSpawn->GetScaledBoxExtent());
	AObstacle* spawnObstacle = GetWorld()->SpawnActor<AObstacle>(ObstacleToSpawn, spawnPosition, GetActorRotation());
	spawnObstacle->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
	// onexited destroy

	

}
void ATile::SpawnPickup()
{
	
	int32 Number = PickupArray.Num() - 1;
	const int RandPickup = FMath::FRandRange(0, Number);
	TSubclassOf<APickup> PickupToSpawn = PickupArray[RandPickup];
	FVector spawnPosition = UKismetMathLibrary::RandomPointInBoundingBox(PickupSpawn->GetComponentLocation(), PickupSpawn->GetScaledBoxExtent());
	APickup* spawnPickup = GetWorld()->SpawnActor<APickup>(PickupToSpawn, spawnPosition, GetActorRotation());
	spawnPickup->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
	//on exited Destroy

}
void ATile::OnExit(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	float Delay = 0.0f;
	ACharacter* RunCharacter = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	if (Spawned == false && RunCharacter == Cast<ACharacter>(OtherActor)) {
		GetWorld()->GetTimerManager().SetTimer(DestroyHandle, this, &ATile::DestroyTile, Delay, false);
	}
	Spawned = true;
	OnExited.Broadcast(this);
	
}

